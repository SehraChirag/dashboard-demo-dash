import dash
import dash_core_components as dcc
import dash_html_components as html
# from pandas_datareader.data import DataReader
import time
import dash_bootstrap_components as dbc
from collections import deque
import plotly.graph_objs as go
import random

external_stylesheets = ["https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css", dbc.themes.BOOTSTRAP]
# external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]
# external_scripts = ['https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js']
# external_scripts=external_scripts,
app = dash.Dash('sensor-data', external_stylesheets=external_stylesheets,  meta_tags=[{"name": "viewport", "content": "width=device-width"}])

max_length = 20
times = deque(maxlen=max_length)
oil_temps = deque(maxlen=max_length)
intake_temps = deque(maxlen=max_length)
coolant_temps = deque(maxlen=max_length)
rpms = deque(maxlen=max_length)
speeds = deque(maxlen=max_length)
throttle_pos = deque(maxlen=max_length)

def update_obd_values(times, oil_temps, intake_temps, coolant_temps, rpms, speeds, throttle_pos):

    times.append(time.time())
    if len(times) == 1:
        #starting relevant values
        oil_temps.append(random.randrange(180,230))
        intake_temps.append(random.randrange(95,115))
        coolant_temps.append(random.randrange(170,220))
        rpms.append(random.randrange(1000,9500))
        speeds.append(random.randrange(30,140))
        throttle_pos.append(random.randrange(10,90))
    else:
        for data_of_interest in [oil_temps, intake_temps, coolant_temps, rpms, speeds, throttle_pos]:
            data_of_interest.append(data_of_interest[-1]+data_of_interest[-1]*random.uniform(-0.0001,0.0001))

    return times, oil_temps, intake_temps, coolant_temps, rpms, speeds, throttle_pos

times, oil_temps, intake_temps, coolant_temps, rpms, speeds, throttle_pos = update_obd_values(times, oil_temps, intake_temps, coolant_temps, rpms, speeds, throttle_pos)


data_dict = {"Oil Temperature":oil_temps,
"Intake Temperature": intake_temps,
"Coolant Temperature": coolant_temps,
"RPM":rpms,
"Speed":speeds,
"Throttle Position":throttle_pos}


def create_card(title, content):
    card = dbc.Card(
        dbc.CardBody(
            [
                html.H4(title, className="card-title"),
                html.Br(),
                html.Br(),
                html.H2(content, className="card-subtitle"),
                html.Br(),
                html.Br(),
                ]
        ),
        color="info", inverse=True
    )
    return(card)

card_1_value,card_2_value,card_3_value,card_4_value = 0,0,0,0

card4 = create_card("Current RPM Acheived", card_1_value)
card3 = create_card("Current Speed Acheived", card_2_value)
card2 = create_card("Current Coolant Temperature", card_3_value)
card1 = create_card("Current Oil Temperature", card_4_value)

graphRow0 = dbc.Row([dbc.Col(id='card1', children=[card1], md=3), dbc.Col(id='card2', children=[card2], md=3), dbc.Col(id='card3', children=[card3], md=3), dbc.Col(id='card4', children=[card4],md=3)])


app.layout = html.Div([
    html.Div([
        ]),
        html.Div(
            [
                html.Br(),
                graphRow0,
            ],
            ),
            html.Br(),

    dcc.Dropdown(id='vehicle-data-name',
                 options=[{'label': s, 'value': s}
                          for s in data_dict.keys()],
                 value=['Coolant Temperature','Intake Temperature','Speed'],
                #  value=['Coolant Temperature','Oil Temperature','Intake Temperature','RPM','Speed','Throttle Position'],
                 multi=True
                 ),
                 
    html.Div(children=html.Div(id='graphs'), className='row'),
    html.Br(),
    dcc.Interval(
        id='graph-update',
        interval=1000),
    ], className="container",style={'width':'98%','margin-left':10,'margin-right':10,'max-width':50000})

# Here are the available properties in "card1":
# ['children', 'id', 'style', 'className', 'key', 'width', 'xs', 'sm', 'md', 'lg', 'xl', 'align', 'loading_state']


@app.callback(
    [
        dash.dependencies.Output('graphs','children'),
        dash.dependencies.Output('card1','children'),
        dash.dependencies.Output('card2','children'),
        dash.dependencies.Output('card3','children'),
        dash.dependencies.Output('card4','children'),
    ],
    [
        dash.dependencies.Input('vehicle-data-name', 'value'),
        dash.dependencies.Input('graph-update', 'n_intervals')
    ]
    )
def update_graph(data_names, n_intervals):
    graphs = []
    update_obd_values(times, oil_temps, intake_temps, coolant_temps, rpms, speeds, throttle_pos)
    if len(data_names)>2:
        class_choice = 'col s12 m6 l4'
    elif len(data_names) == 2:
        class_choice = 'col s12 m6 l6'
    else:
        class_choice = 'col s12'

    card_1_value = round(oil_temps[-1], 3)
    card_2_value = round(coolant_temps[-1],3)
    card_3_value = round(speeds[-1],3)
    card_4_value = round(rpms[-1],3)

    new_card_1 = create_card(" Oil Temperature", card_1_value)
    new_card_2 = create_card(" Coolant Temperature", card_2_value)
    new_card_3 = create_card(" Speed Acheived", card_3_value)
    new_card_4 = create_card(" RPM Acheived", card_4_value)


    for data_name in data_names:

        data = go.Scatter(
            x=list(times),
            y=list(data_dict[data_name]),
            name='Scatter',
            mode='lines+markers',
            )

        graphs.append(html.Div(dcc.Graph(
            id=data_name,
            animate=True,
            figure={'data': [data],'layout' : go.Layout(xaxis=dict(range=[min(times),max(times)]),
                                                        yaxis=dict(range=[min(data_dict[data_name]),max(data_dict[data_name])]),
                                                        margin={'l':50,'r':1,'t':45,'b':1},
                                                        title='{}'.format(data_name))}
            ), className=class_choice))

    return graphs, new_card_1, new_card_2, new_card_3, new_card_4



# external_css = ["https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"]
# for css in external_css:
#     app.css.append_css({"external_url": css})

# external_js = ['https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js']
# for js in external_css:
#     app.scripts.append_script({'external_url': js})


if __name__ == '__main__':
    app.run_server(debug=False)
